<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>LP2D3 - 2022</title>  
  <link rel="icon" href="D:\Programas\XAMPP\htdocs\InacioCarvalho\lp2\assets\mdb\img\mdb-favicon.ico" type="image/x-icon">   
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
  <link rel="stylesheet" href="<?=base_url()?>assets/mdb/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/mdb/css/mdb.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/mdb/css/style.css">

  

  <style type="text/css">
  	
    #body {
		margin: 10% 25% 10% 30%;
	  }    
    #btn{
      margin:auto;      
      min-width: none;
      max-width: 30%;  
    }
    h1{
      text-align: center;  
    }

  
  </style>
</head>
<body>
